<Q                         BLENDMODES_MODE_SATURATION      $  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	mediump vec4 _RendererColor;
uniform 	mediump vec2 _Flip;
uniform 	mediump vec4 _Color;
uniform 	vec4 _BLENDMODES_OverlayTexture_ST;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
out highp vec2 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0.xy = in_POSITION0.xy * _Flip.xy;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0 = in_COLOR0 * _Color;
    u_xlat0 = u_xlat0 * _RendererColor;
    vs_COLOR0 = u_xlat0;
    u_xlat0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD1.xy = u_xlat0.xy * _BLENDMODES_OverlayTexture_ST.xy + _BLENDMODES_OverlayTexture_ST.zw;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _BLENDMODES_OverlayColor;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _BLENDMODES_OverlayTexture;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
in highp vec2 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
mediump vec4 u_xlat16_1;
float u_xlat2;
mediump vec4 u_xlat16_2;
bvec4 u_xlatb2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
bvec3 u_xlatb6;
float u_xlat9;
mediump float u_xlat16_10;
mediump float u_xlat16_12;
mediump float u_xlat16_17;
bool u_xlatb21;
mediump float u_xlat16_24;
mediump float u_xlat16_25;
void main()
{
    u_xlat16_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_1 = u_xlat16_0 * vs_COLOR0;
    u_xlat16_2.xyz = texture(_BLENDMODES_OverlayTexture, vs_TEXCOORD1.xy).xyz;
    u_xlat16_2.xyz = u_xlat16_2.xyz * _BLENDMODES_OverlayColor.xyz;
    u_xlat16_3.x = max(u_xlat16_1.z, u_xlat16_1.y);
    u_xlat16_3.x = max(u_xlat16_1.x, u_xlat16_3.x);
    u_xlat16_10 = min(u_xlat16_1.z, u_xlat16_1.y);
    u_xlat16_10 = min(u_xlat16_1.x, u_xlat16_10);
    u_xlat16_17 = u_xlat16_10 + u_xlat16_3.x;
    u_xlat16_4.xyz = vec3(u_xlat16_17) * vec3(0.5, 0.5, 0.5);
    u_xlat16_24 = max(u_xlat16_2.z, u_xlat16_2.y);
    u_xlat16_24 = max(u_xlat16_2.x, u_xlat16_24);
    u_xlat16_25 = min(u_xlat16_2.z, u_xlat16_2.y);
    u_xlat16_25 = min(u_xlat16_2.x, u_xlat16_25);
    u_xlat16_5.x = u_xlat16_24 + u_xlat16_25;
#ifdef UNITY_ADRENO_ES3
    u_xlatb21 = !!(u_xlat16_24==u_xlat16_25);
#else
    u_xlatb21 = u_xlat16_24==u_xlat16_25;
#endif
    u_xlat16_12 = u_xlat16_24 + (-u_xlat16_25);
#ifdef UNITY_ADRENO_ES3
    u_xlatb2.x = !!(1.0<u_xlat16_5.x);
#else
    u_xlatb2.x = 1.0<u_xlat16_5.x;
#endif
    u_xlat16_24 = (-u_xlat16_24) + (-u_xlat16_25);
    u_xlat16_24 = u_xlat16_24 + 2.0;
    u_xlat16_24 = u_xlat16_12 / u_xlat16_24;
    u_xlat16_25 = u_xlat16_12 / u_xlat16_5.x;
    u_xlat16_24 = (u_xlatb2.x) ? u_xlat16_24 : u_xlat16_25;
    u_xlat16_24 = (u_xlatb21) ? 0.0 : u_xlat16_24;
#ifdef UNITY_ADRENO_ES3
    u_xlatb21 = !!(u_xlat16_24==0.0);
#else
    u_xlatb21 = u_xlat16_24==0.0;
#endif
    if(!u_xlatb21){
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_10==u_xlat16_3.x);
#else
        u_xlatb21 = u_xlat16_10==u_xlat16_3.x;
#endif
        u_xlat16_3.x = (-u_xlat16_10) + u_xlat16_3.x;
        u_xlatb2 = lessThan(u_xlat16_1.yzyz, u_xlat16_1.xxzy);
        u_xlatb2.x = u_xlatb2.y && u_xlatb2.x;
        u_xlat16_5.xyz = u_xlat16_0.yzx * vs_COLOR0.yzx + (-u_xlat16_1.zxy);
        u_xlat16_5.xyz = u_xlat16_5.xyz / u_xlat16_3.xxx;
        u_xlat9 = u_xlatb2.z ? 6.0 : float(0.0);
        u_xlat16_3.x = u_xlat9 + u_xlat16_5.x;
        u_xlat16_5.xy = u_xlat16_5.yz + vec2(2.0, 4.0);
        u_xlat16_10 = (u_xlatb2.w) ? u_xlat16_5.x : u_xlat16_5.y;
        u_xlat16_3.x = (u_xlatb2.x) ? u_xlat16_3.x : u_xlat16_10;
        u_xlat2 = u_xlat16_3.x * 0.166666672;
        u_xlat16_3.x = (u_xlatb21) ? 0.0 : u_xlat2;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_17<1.0);
#else
        u_xlatb21 = u_xlat16_17<1.0;
#endif
        u_xlat16_10 = u_xlat16_24 + 1.0;
        u_xlat16_10 = u_xlat16_10 * u_xlat16_4.z;
        u_xlat16_25 = u_xlat16_17 * 0.5 + u_xlat16_24;
        u_xlat16_24 = (-u_xlat16_4.z) * u_xlat16_24 + u_xlat16_25;
        u_xlat16_10 = (u_xlatb21) ? u_xlat16_10 : u_xlat16_24;
        u_xlat16_17 = (-u_xlat16_10) + u_xlat16_17;
        u_xlat16_2 = u_xlat16_3.xxxx + vec4(0.333333343, 1.33333337, 1.0, -0.333333343);
        u_xlatb6.xy = lessThan(u_xlat16_2.xwxx, vec4(0.0, 0.0, 0.0, 0.0)).xy;
        u_xlat16_24 = (u_xlatb6.x) ? u_xlat16_2.y : u_xlat16_2.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(1.0<u_xlat16_24);
#else
        u_xlatb21 = 1.0<u_xlat16_24;
#endif
        u_xlat16_25 = u_xlat16_24 + -1.0;
        u_xlat16_24 = (u_xlatb21) ? u_xlat16_25 : u_xlat16_24;
        u_xlat16_25 = (-u_xlat16_17) + u_xlat16_10;
        u_xlat16_5.x = u_xlat16_25 * 6.0;
        u_xlat16_12 = u_xlat16_5.x * u_xlat16_24 + u_xlat16_17;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_24>=0.166666672);
#else
        u_xlatb21 = u_xlat16_24>=0.166666672;
#endif
        u_xlatb6.xz = lessThan(vec4(u_xlat16_24), vec4(0.5, 0.0, 0.666666687, 0.0)).xz;
        u_xlat16_24 = (-u_xlat16_24) + 0.666666687;
        u_xlat16_24 = u_xlat16_24 * u_xlat16_25;
        u_xlat16_24 = u_xlat16_24 * 6.0 + u_xlat16_17;
        u_xlat16_24 = (u_xlatb6.z) ? u_xlat16_24 : u_xlat16_17;
        u_xlat16_24 = (u_xlatb6.x) ? u_xlat16_10 : u_xlat16_24;
        u_xlat16_4.x = (u_xlatb21) ? u_xlat16_24 : u_xlat16_12;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_3.x<0.0);
#else
        u_xlatb21 = u_xlat16_3.x<0.0;
#endif
        u_xlat16_24 = (u_xlatb21) ? u_xlat16_2.z : u_xlat16_3.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(1.0<u_xlat16_24);
#else
        u_xlatb21 = 1.0<u_xlat16_24;
#endif
        u_xlat16_12 = u_xlat16_24 + -1.0;
        u_xlat16_24 = (u_xlatb21) ? u_xlat16_12 : u_xlat16_24;
        u_xlat16_12 = u_xlat16_5.x * u_xlat16_24 + u_xlat16_17;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_24>=0.166666672);
#else
        u_xlatb21 = u_xlat16_24>=0.166666672;
#endif
        u_xlatb6.xz = lessThan(vec4(u_xlat16_24), vec4(0.5, 0.0, 0.666666687, 0.0)).xz;
        u_xlat16_24 = (-u_xlat16_24) + 0.666666687;
        u_xlat16_24 = u_xlat16_24 * u_xlat16_25;
        u_xlat16_24 = u_xlat16_24 * 6.0 + u_xlat16_17;
        u_xlat16_24 = (u_xlatb6.z) ? u_xlat16_24 : u_xlat16_17;
        u_xlat16_24 = (u_xlatb6.x) ? u_xlat16_10 : u_xlat16_24;
        u_xlat16_4.y = (u_xlatb21) ? u_xlat16_24 : u_xlat16_12;
        u_xlat16_3.x = u_xlat16_3.x + 0.666666627;
        u_xlat16_3.x = (u_xlatb6.y) ? u_xlat16_3.x : u_xlat16_2.w;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(1.0<u_xlat16_3.x);
#else
        u_xlatb21 = 1.0<u_xlat16_3.x;
#endif
        u_xlat16_24 = u_xlat16_3.x + -1.0;
        u_xlat16_3.x = (u_xlatb21) ? u_xlat16_24 : u_xlat16_3.x;
        u_xlat16_24 = u_xlat16_5.x * u_xlat16_3.x + u_xlat16_17;
#ifdef UNITY_ADRENO_ES3
        u_xlatb21 = !!(u_xlat16_3.x>=0.166666672);
#else
        u_xlatb21 = u_xlat16_3.x>=0.166666672;
#endif
        u_xlatb6.xy = lessThan(u_xlat16_3.xxxx, vec4(0.5, 0.666666687, 0.0, 0.0)).xy;
        u_xlat16_3.x = (-u_xlat16_3.x) + 0.666666687;
        u_xlat16_3.x = u_xlat16_3.x * u_xlat16_25;
        u_xlat16_3.x = u_xlat16_3.x * 6.0 + u_xlat16_17;
        u_xlat16_3.x = (u_xlatb6.y) ? u_xlat16_3.x : u_xlat16_17;
        u_xlat16_3.x = (u_xlatb6.x) ? u_xlat16_10 : u_xlat16_3.x;
        u_xlat16_4.z = (u_xlatb21) ? u_xlat16_3.x : u_xlat16_24;
    }
    u_xlat16_3.xyz = (-u_xlat16_0.xyz) * vs_COLOR0.xyz + u_xlat16_4.xyz;
    u_xlat16_1.xyz = _BLENDMODES_OverlayColor.www * u_xlat16_3.xyz + u_xlat16_1.xyz;
    SV_Target0.xyz = u_xlat16_1.www * u_xlat16_1.xyz;
    SV_Target0.w = u_xlat16_1.w;
    return;
}

#endif
                               $Globals         _BLENDMODES_OverlayColor                             $Globals�         _MainTex_ST                   �      _RendererColor                    �      _Flip                     �      _Color                    �      _BLENDMODES_OverlayTexture_ST                     �      unity_ObjectToWorld                         unity_MatrixVP                   @             _MainTex                  _BLENDMODES_OverlayTexture               