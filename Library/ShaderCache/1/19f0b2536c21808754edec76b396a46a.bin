<Q                         BLENDMODES_MODE_VIVIDLIGHT     ETC1_EXTERNAL_ALPHA    PIXELSNAP_ON    �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _RendererColor;
uniform 	mediump vec2 _Flip;
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_VCOLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_VCOLOR0;
out highp vec2 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0.xy = in_POSITION0.xy * _Flip.xy;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = _ScreenParams.xy * vec2(0.5, 0.5);
    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
    u_xlat0.xy = roundEven(u_xlat0.xy);
    u_xlat0.xy = u_xlat0.xy / u_xlat1.xy;
    gl_Position.xy = u_xlat0.ww * u_xlat0.xy;
    gl_Position.zw = u_xlat0.zw;
    u_xlat0 = in_VCOLOR0 * _Color;
    u_xlat0 = u_xlat0 * _RendererColor;
    vs_VCOLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_framebuffer_fetch
#extension GL_EXT_shader_framebuffer_fetch : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	float _EnableExternalAlpha;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _AlphaTex;
in mediump vec4 vs_VCOLOR0;
in highp vec2 vs_TEXCOORD0;
#ifdef GL_EXT_shader_framebuffer_fetch
layout(location = 0) inout mediump vec4 SV_Target0;
#else
layout(location = 0) out mediump vec4 SV_Target0;
#endif
mediump vec3 u_xlat16_0;
float u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
bvec3 u_xlatb2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
void main()
{
    u_xlat16_0.xyz = (-SV_Target0.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat16_1.x = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat1 = u_xlat16_1.x + (-u_xlat2.w);
    u_xlat2.w = _EnableExternalAlpha * u_xlat1 + u_xlat2.w;
    u_xlat16_1 = u_xlat2 * vs_VCOLOR0;
    u_xlat16_3.xyz = u_xlat2.xyz * vs_VCOLOR0.xyz + vec3(-0.5, -0.5, -0.5);
    u_xlat16_3.xyz = (-u_xlat16_3.xyz) * vec3(2.0, 2.0, 2.0) + vec3(1.0, 1.0, 1.0);
    u_xlat16_3.xyz = SV_Target0.xyz / u_xlat16_3.xyz;
    u_xlat16_4.xyz = u_xlat16_1.xyz + u_xlat16_1.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xyz / u_xlat16_4.xyz;
    u_xlat16_0.xyz = (-u_xlat16_0.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlatb2.xyz = lessThan(vec4(0.5, 0.5, 0.5, 0.0), u_xlat16_1.xyzx).xyz;
    {
        vec3 hlslcc_movcTemp = u_xlat16_0;
        hlslcc_movcTemp.x = (u_xlatb2.x) ? u_xlat16_3.x : u_xlat16_0.x;
        hlslcc_movcTemp.y = (u_xlatb2.y) ? u_xlat16_3.y : u_xlat16_0.y;
        hlslcc_movcTemp.z = (u_xlatb2.z) ? u_xlat16_3.z : u_xlat16_0.z;
        u_xlat16_0 = hlslcc_movcTemp;
    }
#ifdef UNITY_ADRENO_ES3
    u_xlat16_0.xyz = min(max(u_xlat16_0.xyz, 0.0), 1.0);
#else
    u_xlat16_0.xyz = clamp(u_xlat16_0.xyz, 0.0, 1.0);
#endif
    SV_Target0.xyz = u_xlat16_1.www * u_xlat16_0.xyz;
    SV_Target0.w = u_xlat16_1.w;
    return;
}

#endif
                               $Globals         _EnableExternalAlpha                             $Globals�         _ScreenParams                            _RendererColor                    �      _Flip                     �      _Color                    �      unity_ObjectToWorld                        unity_MatrixVP                   P             _MainTex               	   _AlphaTex                