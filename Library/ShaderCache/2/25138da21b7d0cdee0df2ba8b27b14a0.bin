<Q                         BLENDMODES_MODE_LINEARLIGHT     �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	mediump vec4 _RendererColor;
uniform 	mediump vec2 _Flip;
uniform 	mediump vec4 _Color;
uniform 	vec4 _BLENDMODES_OverlayTexture_ST;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
out highp vec2 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0.xy = in_POSITION0.xy * _Flip.xy;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0 = in_COLOR0 * _Color;
    u_xlat0 = u_xlat0 * _RendererColor;
    vs_COLOR0 = u_xlat0;
    u_xlat0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD1.xy = u_xlat0.xy * _BLENDMODES_OverlayTexture_ST.xy + _BLENDMODES_OverlayTexture_ST.zw;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _BLENDMODES_OverlayColor;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _BLENDMODES_OverlayTexture;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
in highp vec2 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec3 u_xlat16_0;
bvec3 u_xlatb0;
vec3 u_xlat1;
mediump vec3 u_xlat16_2;
mediump vec4 u_xlat16_3;
mediump vec4 u_xlat16_4;
mediump vec3 u_xlat16_5;
void main()
{
    u_xlat16_0.xyz = texture(_BLENDMODES_OverlayTexture, vs_TEXCOORD1.xy).xyz;
    u_xlat1.xyz = u_xlat16_0.xyz * _BLENDMODES_OverlayColor.xyz;
    u_xlat16_2.xyz = u_xlat16_0.xyz * _BLENDMODES_OverlayColor.xyz + vec3(-0.5, -0.5, -0.5);
    u_xlatb0.xyz = lessThan(vec4(0.5, 0.5, 0.5, 0.0), u_xlat1.xyzx).xyz;
    u_xlat16_3 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_4 = u_xlat16_3 * vs_COLOR0;
    u_xlat16_5.xyz = u_xlat1.xyz * vec3(2.0, 2.0, 2.0) + u_xlat16_4.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyz + vec3(-1.0, -1.0, -1.0);
    u_xlat16_2.xyz = u_xlat16_2.xyz * vec3(2.0, 2.0, 2.0) + u_xlat16_4.xyz;
    {
        vec3 hlslcc_movcTemp = u_xlat16_2;
        hlslcc_movcTemp.x = (u_xlatb0.x) ? u_xlat16_2.x : u_xlat16_5.x;
        hlslcc_movcTemp.y = (u_xlatb0.y) ? u_xlat16_2.y : u_xlat16_5.y;
        hlslcc_movcTemp.z = (u_xlatb0.z) ? u_xlat16_2.z : u_xlat16_5.z;
        u_xlat16_2 = hlslcc_movcTemp;
    }
    u_xlat16_2.xyz = (-u_xlat16_3.xyz) * vs_COLOR0.xyz + u_xlat16_2.xyz;
    u_xlat16_2.xyz = _BLENDMODES_OverlayColor.www * u_xlat16_2.xyz + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_4.www * u_xlat16_2.xyz;
    SV_Target0.w = u_xlat16_4.w;
    return;
}

#endif
                                $Globals         _BLENDMODES_OverlayColor                             $Globals�         _MainTex_ST                   �      _RendererColor                    �      _Flip                     �      _Color                    �      _BLENDMODES_OverlayTexture_ST                     �      unity_ObjectToWorld                         unity_MatrixVP                   @             _MainTex                  _BLENDMODES_OverlayTexture               