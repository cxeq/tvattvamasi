<Q                         BLENDMODES_MODE_PINLIGHT   ETC1_EXTERNAL_ALPHA    PIXELSNAP_ON    �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	mediump vec4 _RendererColor;
uniform 	mediump vec2 _Flip;
uniform 	mediump vec4 _Color;
uniform 	vec4 _BLENDMODES_OverlayTexture_ST;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
out highp vec2 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0.xy = in_POSITION0.xy * _Flip.xy;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = _ScreenParams.xy * vec2(0.5, 0.5);
    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
    u_xlat0.xy = roundEven(u_xlat0.xy);
    u_xlat0.xy = u_xlat0.xy / u_xlat1.xy;
    gl_Position.xy = u_xlat0.ww * u_xlat0.xy;
    gl_Position.zw = u_xlat0.zw;
    u_xlat0 = in_COLOR0 * _Color;
    u_xlat0 = u_xlat0 * _RendererColor;
    vs_COLOR0 = u_xlat0;
    u_xlat0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD1.xy = u_xlat0.xy * _BLENDMODES_OverlayTexture_ST.xy + _BLENDMODES_OverlayTexture_ST.zw;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	float _EnableExternalAlpha;
uniform 	mediump vec4 _BLENDMODES_OverlayColor;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _AlphaTex;
UNITY_LOCATION(2) uniform mediump sampler2D _BLENDMODES_OverlayTexture;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
in highp vec2 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec3 u_xlat16_0;
bvec3 u_xlatb0;
mediump vec3 u_xlat16_1;
vec4 u_xlat2;
mediump vec4 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat15;
mediump float u_xlat16_15;
void main()
{
    u_xlat16_0.xyz = texture(_BLENDMODES_OverlayTexture, vs_TEXCOORD1.xy).xyz;
    u_xlat16_1.xyz = u_xlat16_0.xyz * _BLENDMODES_OverlayColor.xyz + vec3(-0.5, -0.5, -0.5);
    u_xlat0.xyz = u_xlat16_0.xyz * _BLENDMODES_OverlayColor.xyz;
    u_xlat16_1.xyz = u_xlat16_1.xyz + u_xlat16_1.xyz;
    u_xlat16_15 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat15 = u_xlat16_15 + (-u_xlat2.w);
    u_xlat2.w = _EnableExternalAlpha * u_xlat15 + u_xlat2.w;
    u_xlat16_3 = u_xlat2 * vs_COLOR0;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, u_xlat16_3.xyz);
    u_xlat16_4.xyz = u_xlat0.xyz + u_xlat0.xyz;
    u_xlatb0.xyz = lessThan(vec4(0.5, 0.5, 0.5, 0.0), u_xlat0.xyzx).xyz;
    u_xlat16_4.xyz = min(u_xlat16_3.xyz, u_xlat16_4.xyz);
    {
        vec3 hlslcc_movcTemp = u_xlat16_1;
        hlslcc_movcTemp.x = (u_xlatb0.x) ? u_xlat16_1.x : u_xlat16_4.x;
        hlslcc_movcTemp.y = (u_xlatb0.y) ? u_xlat16_1.y : u_xlat16_4.y;
        hlslcc_movcTemp.z = (u_xlatb0.z) ? u_xlat16_1.z : u_xlat16_4.z;
        u_xlat16_1 = hlslcc_movcTemp;
    }
    u_xlat16_1.xyz = (-u_xlat2.xyz) * vs_COLOR0.xyz + u_xlat16_1.xyz;
    u_xlat16_1.xyz = _BLENDMODES_OverlayColor.www * u_xlat16_1.xyz + u_xlat16_3.xyz;
    SV_Target0.xyz = u_xlat16_3.www * u_xlat16_1.xyz;
    SV_Target0.w = u_xlat16_3.w;
    return;
}

#endif
                               $Globals          _EnableExternalAlpha                         _BLENDMODES_OverlayColor                            $Globals�         _ScreenParams                            _MainTex_ST                   �      _RendererColor                    �      _Flip                     �      _Color                    �      _BLENDMODES_OverlayTexture_ST                     �      unity_ObjectToWorld                        unity_MatrixVP                   P             _MainTex               	   _AlphaTex                   _BLENDMODES_OverlayTexture               