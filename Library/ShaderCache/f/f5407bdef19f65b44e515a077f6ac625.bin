<Q                         USE_SHAPE_LIGHT_TYPE_3      _h  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec3 in_POSITION0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_COLOR0;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    gl_Position = u_xlat0;
    vs_TEXCOORD0 = in_TEXCOORD0;
    vs_TEXCOORD1 = in_COLOR0;
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD2.zw = u_xlat0.zw;
    vs_TEXCOORD2.xy = u_xlat1.zz + u_xlat1.xw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _TimeParameters;
uniform 	vec2 _ShapeLightBlendFactors3;
uniform 	vec4 _ShapeLightMaskFilter3;
uniform 	vec4 _ShapeLightInvertedFilter3;
uniform 	mediump float _HDREmulationScale;
uniform 	mediump float _UseSceneLighting;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM float _DissolveAmount;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _ShapeLightTexture3;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_TARGET0;
vec4 u_xlat0;
bvec2 u_xlatb0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
bool u_xlatb1;
vec4 u_xlat2;
bvec4 u_xlatb2;
vec4 u_xlat3;
bvec4 u_xlatb3;
vec4 u_xlat4;
bvec4 u_xlatb4;
vec4 u_xlat5;
bvec4 u_xlatb5;
vec4 u_xlat6;
bvec4 u_xlatb6;
vec4 u_xlat7;
mediump vec3 u_xlat16_8;
vec3 u_xlat9;
vec3 u_xlat10;
vec3 u_xlat11;
bool u_xlatb11;
vec2 u_xlat18;
vec2 u_xlat19;
bool u_xlatb19;
vec2 u_xlat20;
bool u_xlatb20;
vec2 u_xlat21;
bool u_xlatb21;
vec2 u_xlat22;
bool u_xlatb22;
float u_xlat27;
float u_xlat28;
bool u_xlatb28;
float u_xlat29;
bool u_xlatb29;
void main()
{
    u_xlat0 = vs_TEXCOORD0.xyxy * vec4(26.5, 26.5, 8.0, 8.0);
    u_xlat1 = floor(u_xlat0);
    u_xlat0 = fract(u_xlat0);
    u_xlat2 = u_xlat1.zwzw * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb2 = greaterThanEqual(u_xlat2, (-u_xlat2.zwzw));
    u_xlat2.x = (u_xlatb2.x) ? float(289.0) : float(-289.0);
    u_xlat2.y = (u_xlatb2.y) ? float(289.0) : float(-289.0);
    u_xlat2.z = (u_xlatb2.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat2.w = (u_xlatb2.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat20.xy = u_xlat1.zw * u_xlat2.zw;
    u_xlat20.xy = fract(u_xlat20.xy);
    u_xlat2.xy = u_xlat20.xy * u_xlat2.xy;
    u_xlat20.x = u_xlat2.x * 34.0 + 1.0;
    u_xlat2.x = u_xlat2.x * u_xlat20.x;
    u_xlat20.x = u_xlat2.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat20.x>=(-u_xlat20.x));
#else
    u_xlatb20 = u_xlat20.x>=(-u_xlat20.x);
#endif
    u_xlat20.xy = (bool(u_xlatb20)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat2.x = u_xlat20.y * u_xlat2.x;
    u_xlat2.x = fract(u_xlat2.x);
    u_xlat2.x = u_xlat20.x * u_xlat2.x + u_xlat2.y;
    u_xlat11.x = u_xlat2.x * 34.0 + 1.0;
    u_xlat2.x = u_xlat2.x * u_xlat11.x;
    u_xlat11.x = u_xlat2.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(u_xlat11.x>=(-u_xlat11.x));
#else
    u_xlatb11 = u_xlat11.x>=(-u_xlat11.x);
#endif
    u_xlat11.xy = (bool(u_xlatb11)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat2.x = u_xlat11.y * u_xlat2.x;
    u_xlat2.x = fract(u_xlat2.x);
    u_xlat2.x = u_xlat2.x * u_xlat11.x;
    u_xlat2.x = u_xlat2.x * 0.024390243;
    u_xlat2.x = fract(u_xlat2.x);
    u_xlat2.xy = u_xlat2.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat11.x = floor(u_xlat2.y);
    u_xlat3.x = (-u_xlat11.x) + u_xlat2.x;
    u_xlat3.y = abs(u_xlat2.x) + -0.5;
    u_xlat2.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xy = u_xlat2.xx * u_xlat3.xy;
    u_xlat2.x = dot(u_xlat2.xy, u_xlat0.zw);
    u_xlat3 = u_xlat1 + vec4(1.0, 1.0, 0.0, 1.0);
    u_xlat4 = u_xlat3.zwzw * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb4 = greaterThanEqual(u_xlat4, (-u_xlat4.zwzw));
    u_xlat4.x = (u_xlatb4.x) ? float(289.0) : float(-289.0);
    u_xlat4.y = (u_xlatb4.y) ? float(289.0) : float(-289.0);
    u_xlat4.z = (u_xlatb4.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.w = (u_xlatb4.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat11.xy = u_xlat3.zw * u_xlat4.zw;
    u_xlat29 = dot(u_xlat3.xy, vec2(12.9898005, 78.2330017));
    u_xlat29 = sin(u_xlat29);
    u_xlat11.z = u_xlat29 * 43758.5469;
    u_xlat11.xyz = fract(u_xlat11.xyz);
    u_xlat11.xy = u_xlat11.xy * u_xlat4.xy;
    u_xlat3.x = u_xlat11.x * 34.0 + 1.0;
    u_xlat11.x = u_xlat11.x * u_xlat3.x;
    u_xlat3.x = u_xlat11.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(u_xlat3.x>=(-u_xlat3.x));
#else
    u_xlatb3.x = u_xlat3.x>=(-u_xlat3.x);
#endif
    u_xlat3.xy = (u_xlatb3.x) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat11.x = u_xlat11.x * u_xlat3.y;
    u_xlat11.x = fract(u_xlat11.x);
    u_xlat11.x = u_xlat3.x * u_xlat11.x + u_xlat11.y;
    u_xlat20.x = u_xlat11.x * 34.0 + 1.0;
    u_xlat11.x = u_xlat11.x * u_xlat20.x;
    u_xlat20.x = u_xlat11.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat20.x>=(-u_xlat20.x));
#else
    u_xlatb20 = u_xlat20.x>=(-u_xlat20.x);
#endif
    u_xlat3.xy = (bool(u_xlatb20)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat11.x = u_xlat11.x * u_xlat3.y;
    u_xlat11.x = fract(u_xlat11.x);
    u_xlat11.x = u_xlat11.x * u_xlat3.x;
    u_xlat11.x = u_xlat11.x * 0.024390243;
    u_xlat11.x = fract(u_xlat11.x);
    u_xlat11.xy = u_xlat11.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat20.x = floor(u_xlat11.y);
    u_xlat3.x = (-u_xlat20.x) + u_xlat11.x;
    u_xlat3.y = abs(u_xlat11.x) + -0.5;
    u_xlat11.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat11.x = inversesqrt(u_xlat11.x);
    u_xlat11.xy = u_xlat11.xx * u_xlat3.xy;
    u_xlat3 = u_xlat0.zwzw + vec4(-0.0, -1.0, -1.0, -0.0);
    u_xlat11.x = dot(u_xlat11.xy, u_xlat3.xy);
    u_xlat11.x = (-u_xlat2.x) + u_xlat11.x;
    u_xlat3.xy = u_xlat0.zw * vec2(6.0, 6.0) + vec2(-15.0, -15.0);
    u_xlat3.xy = u_xlat0.zw * u_xlat3.xy + vec2(10.0, 10.0);
    u_xlat4 = u_xlat0 * u_xlat0;
    u_xlat22.xy = u_xlat0.zw * u_xlat4.zw;
    u_xlat3.xy = u_xlat3.xy * u_xlat22.xy;
    u_xlat2.x = u_xlat3.y * u_xlat11.x + u_xlat2.x;
    u_xlat5 = u_xlat1.zwzw + vec4(1.0, 0.0, 1.0, 1.0);
    u_xlat6 = u_xlat5 * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb6 = greaterThanEqual(u_xlat6, (-u_xlat6));
    u_xlat7.x = (u_xlatb6.z) ? float(289.0) : float(-289.0);
    u_xlat7.y = (u_xlatb6.w) ? float(289.0) : float(-289.0);
    u_xlat7.z = (u_xlatb6.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat7.w = (u_xlatb6.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat6.x = (u_xlatb6.x) ? float(289.0) : float(-289.0);
    u_xlat6.y = (u_xlatb6.y) ? float(289.0) : float(-289.0);
    u_xlat6.z = (u_xlatb6.x) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat6.w = (u_xlatb6.y) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat19.xy = u_xlat5.zw * u_xlat7.zw;
    u_xlat11.xy = u_xlat5.xy * u_xlat6.zw;
    u_xlat11.xy = fract(u_xlat11.xy);
    u_xlat11.xy = u_xlat11.xy * u_xlat6.xy;
    u_xlat19.xy = fract(u_xlat19.xy);
    u_xlat19.xy = u_xlat19.xy * u_xlat7.xy;
    u_xlat22.x = u_xlat19.x * 34.0 + 1.0;
    u_xlat19.x = u_xlat19.x * u_xlat22.x;
    u_xlat22.x = u_xlat19.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb22 = !!(u_xlat22.x>=(-u_xlat22.x));
#else
    u_xlatb22 = u_xlat22.x>=(-u_xlat22.x);
#endif
    u_xlat22.xy = (bool(u_xlatb22)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat19.x = u_xlat19.x * u_xlat22.y;
    u_xlat19.x = fract(u_xlat19.x);
    u_xlat19.x = u_xlat22.x * u_xlat19.x + u_xlat19.y;
    u_xlat28 = u_xlat19.x * 34.0 + 1.0;
    u_xlat19.x = u_xlat19.x * u_xlat28;
    u_xlat28 = u_xlat19.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb28 = !!(u_xlat28>=(-u_xlat28));
#else
    u_xlatb28 = u_xlat28>=(-u_xlat28);
#endif
    u_xlat22.xy = (bool(u_xlatb28)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat19.x = u_xlat19.x * u_xlat22.y;
    u_xlat19.x = fract(u_xlat19.x);
    u_xlat19.x = u_xlat19.x * u_xlat22.x;
    u_xlat19.x = u_xlat19.x * 0.024390243;
    u_xlat19.x = fract(u_xlat19.x);
    u_xlat19.xy = u_xlat19.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat28 = floor(u_xlat19.y);
    u_xlat5.x = (-u_xlat28) + u_xlat19.x;
    u_xlat5.y = abs(u_xlat19.x) + -0.5;
    u_xlat19.x = dot(u_xlat5.xy, u_xlat5.xy);
    u_xlat19.x = inversesqrt(u_xlat19.x);
    u_xlat19.xy = u_xlat19.xx * u_xlat5.xy;
    u_xlat18.xy = u_xlat0.zw + vec2(-1.0, -1.0);
    u_xlat0.xy = (-u_xlat0.xy) * vec2(2.0, 2.0) + vec2(3.0, 3.0);
    u_xlat18.x = dot(u_xlat19.xy, u_xlat18.xy);
    u_xlat27 = u_xlat11.x * 34.0 + 1.0;
    u_xlat27 = u_xlat11.x * u_xlat27;
    u_xlat19.x = u_xlat27 * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb19 = !!(u_xlat19.x>=(-u_xlat19.x));
#else
    u_xlatb19 = u_xlat19.x>=(-u_xlat19.x);
#endif
    u_xlat19.xy = (bool(u_xlatb19)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat27 = u_xlat27 * u_xlat19.y;
    u_xlat27 = fract(u_xlat27);
    u_xlat27 = u_xlat19.x * u_xlat27 + u_xlat11.y;
    u_xlat19.x = u_xlat27 * 34.0 + 1.0;
    u_xlat27 = u_xlat27 * u_xlat19.x;
    u_xlat19.x = u_xlat27 * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb19 = !!(u_xlat19.x>=(-u_xlat19.x));
#else
    u_xlatb19 = u_xlat19.x>=(-u_xlat19.x);
#endif
    u_xlat19.xy = (bool(u_xlatb19)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat27 = u_xlat27 * u_xlat19.y;
    u_xlat27 = fract(u_xlat27);
    u_xlat27 = u_xlat27 * u_xlat19.x;
    u_xlat27 = u_xlat27 * 0.024390243;
    u_xlat27 = fract(u_xlat27);
    u_xlat19.xy = vec2(u_xlat27) * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat27 = floor(u_xlat19.y);
    u_xlat5.x = (-u_xlat27) + u_xlat19.x;
    u_xlat5.y = abs(u_xlat19.x) + -0.5;
    u_xlat27 = dot(u_xlat5.xy, u_xlat5.xy);
    u_xlat27 = inversesqrt(u_xlat27);
    u_xlat19.xy = vec2(u_xlat27) * u_xlat5.xy;
    u_xlat27 = dot(u_xlat19.xy, u_xlat3.zw);
    u_xlat18.x = (-u_xlat27) + u_xlat18.x;
    u_xlat18.x = u_xlat3.y * u_xlat18.x + u_xlat27;
    u_xlat18.x = (-u_xlat2.x) + u_xlat18.x;
    u_xlat18.x = u_xlat3.x * u_xlat18.x + u_xlat2.x;
    u_xlat18.x = u_xlat18.x + 0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat18.x = min(max(u_xlat18.x, 0.0), 1.0);
#else
    u_xlat18.x = clamp(u_xlat18.x, 0.0, 1.0);
#endif
    u_xlat3.xyz = u_xlat18.xxx * vec3(0.828303337, -0.528213263, -0.297994971) + vec3(0.0237759501, 0.706276298, 1.51571703);
    u_xlat18.x = dot(u_xlat1.xy, vec2(12.9898005, 78.2330017));
    u_xlat1 = u_xlat1.xyxy + vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat18.x = sin(u_xlat18.x);
    u_xlat18.x = u_xlat18.x * 43758.5469;
    u_xlat27 = dot(u_xlat1.xy, vec2(12.9898005, 78.2330017));
    u_xlat1.x = dot(u_xlat1.zw, vec2(12.9898005, 78.2330017));
    u_xlat1.x = sin(u_xlat1.x);
    u_xlat1.x = u_xlat1.x * 43758.5469;
    u_xlat1.x = fract(u_xlat1.x);
    u_xlat27 = sin(u_xlat27);
    u_xlat18.y = u_xlat27 * 43758.5469;
    u_xlat18.xy = fract(u_xlat18.xy);
    u_xlat10.xy = u_xlat0.xy * u_xlat4.xy;
    u_xlat0.xy = (-u_xlat4.xy) * u_xlat0.xy + vec2(1.0, 1.0);
    u_xlat27 = u_xlat18.y * u_xlat10.x;
    u_xlat18.x = u_xlat0.x * u_xlat18.x + u_xlat27;
    u_xlat27 = u_xlat11.z * u_xlat10.x;
    u_xlat0.x = u_xlat0.x * u_xlat1.x + u_xlat27;
    u_xlat0.x = u_xlat0.x * u_xlat10.y;
    u_xlat0.x = u_xlat0.y * u_xlat18.x + u_xlat0.x;
    u_xlat1 = vs_TEXCOORD0.xyxy * vec4(106.0, 106.0, 53.0, 53.0);
    u_xlat2 = floor(u_xlat1);
    u_xlat1 = fract(u_xlat1);
    u_xlat4 = u_xlat2.zwzw + vec4(0.0, 1.0, 1.0, 1.0);
    u_xlat9.x = dot(u_xlat4.xy, vec2(12.9898005, 78.2330017));
    u_xlat9.y = dot(u_xlat4.zw, vec2(12.9898005, 78.2330017));
    u_xlat9.xy = sin(u_xlat9.xy);
    u_xlat9.xy = u_xlat9.xy * vec2(43758.5469, 43758.5469);
    u_xlat9.xy = fract(u_xlat9.xy);
    u_xlat4 = u_xlat1 * u_xlat1;
    u_xlat1 = (-u_xlat1) * vec4(2.0, 2.0, 2.0, 2.0) + vec4(3.0, 3.0, 3.0, 3.0);
    u_xlat5 = u_xlat1 * u_xlat4;
    u_xlat1 = (-u_xlat4) * u_xlat1 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat18.x = u_xlat9.y * u_xlat5.z;
    u_xlat9.x = u_xlat1.z * u_xlat9.x + u_xlat18.x;
    u_xlat18.x = dot(u_xlat2.zw, vec2(12.9898005, 78.2330017));
    u_xlat18.x = sin(u_xlat18.x);
    u_xlat18.x = u_xlat18.x * 43758.5469;
    u_xlat4 = u_xlat2 + vec4(1.0, 1.0, 1.0, 0.0);
    u_xlat27 = dot(u_xlat4.zw, vec2(12.9898005, 78.2330017));
    u_xlat20.x = dot(u_xlat4.xy, vec2(12.9898005, 78.2330017));
    u_xlat20.x = sin(u_xlat20.x);
    u_xlat20.x = u_xlat20.x * 43758.5469;
    u_xlat20.x = fract(u_xlat20.x);
    u_xlat20.x = u_xlat20.x * u_xlat5.x;
    u_xlat27 = sin(u_xlat27);
    u_xlat18.y = u_xlat27 * 43758.5469;
    u_xlat9.yz = fract(u_xlat18.xy);
    u_xlat9.xz = u_xlat9.xz * u_xlat5.wz;
    u_xlat18.x = u_xlat1.z * u_xlat9.y + u_xlat9.z;
    u_xlat9.x = u_xlat1.w * u_xlat18.x + u_xlat9.x;
    u_xlat18.x = dot(u_xlat2.xy, vec2(12.9898005, 78.2330017));
    u_xlat4 = u_xlat2.xyxy + vec4(1.0, 0.0, 0.0, 1.0);
    u_xlat9.y = sin(u_xlat18.x);
    u_xlat9.xy = u_xlat9.xy * vec2(0.25, 43758.5469);
    u_xlat27 = dot(u_xlat4.xy, vec2(12.9898005, 78.2330017));
    u_xlat19.x = dot(u_xlat4.zw, vec2(12.9898005, 78.2330017));
    u_xlat19.x = sin(u_xlat19.x);
    u_xlat19.x = u_xlat19.x * 43758.5469;
    u_xlat19.x = fract(u_xlat19.x);
    u_xlat19.x = u_xlat1.x * u_xlat19.x + u_xlat20.x;
    u_xlat19.x = u_xlat19.x * u_xlat5.y;
    u_xlat27 = sin(u_xlat27);
    u_xlat9.z = u_xlat27 * 43758.5469;
    u_xlat18.xy = fract(u_xlat9.yz);
    u_xlat27 = u_xlat18.y * u_xlat5.x;
    u_xlat18.x = u_xlat1.x * u_xlat18.x + u_xlat27;
    u_xlat18.x = u_xlat1.y * u_xlat18.x + u_xlat19.x;
    u_xlat9.x = u_xlat18.x * 0.125 + u_xlat9.x;
    u_xlat0.x = u_xlat0.x * 0.5 + u_xlat9.x;
    u_xlat9.xy = (-vec2(_DissolveAmount)) + vec2(1.0, 1.10000002);
    u_xlatb0.xy = greaterThanEqual(u_xlat9.xyxx, u_xlat0.xxxx).xy;
    u_xlat18.x = (u_xlatb0.x) ? -1.0 : -0.0;
    u_xlat0.x = u_xlatb0.x ? float(1.0) : 0.0;
    u_xlat0.y = u_xlatb0.y ? float(1.0) : 0.0;
;
    u_xlat9.x = u_xlat18.x + u_xlat0.y;
    u_xlat16_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat9.x = u_xlat9.x * u_xlat16_1.w;
    u_xlat2.w = u_xlat0.x * u_xlat16_1.w;
    u_xlat3.w = 1.0;
    u_xlat2.xyz = u_xlat16_1.xyz;
    u_xlat0 = u_xlat9.xxxx * u_xlat3 + u_xlat2;
    u_xlat1.xy = vs_TEXCOORD0.xy * vec2(6.0, 6.0);
    u_xlat2.xy = floor(u_xlat1.xy);
    u_xlat20.xy = u_xlat2.xy + vec2(1.0, 1.0);
    u_xlat3 = u_xlat20.xyxy * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb3 = greaterThanEqual(u_xlat3, (-u_xlat3.zwzw));
    u_xlat3.x = (u_xlatb3.x) ? float(289.0) : float(-289.0);
    u_xlat3.y = (u_xlatb3.y) ? float(289.0) : float(-289.0);
    u_xlat3.z = (u_xlatb3.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat3.w = (u_xlatb3.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat20.xy = u_xlat20.xy * u_xlat3.zw;
    u_xlat20.xy = fract(u_xlat20.xy);
    u_xlat20.xy = u_xlat20.xy * u_xlat3.xy;
    u_xlat19.x = u_xlat20.x * 34.0 + 1.0;
    u_xlat19.x = u_xlat20.x * u_xlat19.x;
    u_xlat20.x = u_xlat19.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat20.x>=(-u_xlat20.x));
#else
    u_xlatb20 = u_xlat20.x>=(-u_xlat20.x);
#endif
    u_xlat3.xy = (bool(u_xlatb20)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat1.z = u_xlat19.x * u_xlat3.y;
    u_xlat1.xyz = fract(u_xlat1.xyz);
    u_xlat19.x = u_xlat3.x * u_xlat1.z + u_xlat20.y;
    u_xlat20.x = u_xlat19.x * 34.0 + 1.0;
    u_xlat19.x = u_xlat19.x * u_xlat20.x;
    u_xlat20.x = u_xlat19.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat20.x>=(-u_xlat20.x));
#else
    u_xlatb20 = u_xlat20.x>=(-u_xlat20.x);
#endif
    u_xlat20.xy = (bool(u_xlatb20)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat19.x = u_xlat19.x * u_xlat20.y;
    u_xlat19.x = fract(u_xlat19.x);
    u_xlat19.x = u_xlat19.x * u_xlat20.x;
    u_xlat19.x = u_xlat19.x * 0.024390243;
    u_xlat19.x = fract(u_xlat19.x);
    u_xlat20.xy = u_xlat19.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat19.x = floor(u_xlat20.y);
    u_xlat3.x = (-u_xlat19.x) + u_xlat20.x;
    u_xlat3.y = abs(u_xlat20.x) + -0.5;
    u_xlat19.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat19.x = inversesqrt(u_xlat19.x);
    u_xlat20.xy = u_xlat19.xx * u_xlat3.xy;
    u_xlat3.xy = u_xlat1.xy + vec2(-1.0, -1.0);
    u_xlat19.x = dot(u_xlat20.xy, u_xlat3.xy);
    u_xlat3 = u_xlat2.xyxy + vec4(0.0, 1.0, 1.0, 0.0);
    u_xlat4 = u_xlat3 * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb4 = greaterThanEqual(u_xlat4, (-u_xlat4));
    u_xlat5.x = (u_xlatb4.z) ? float(289.0) : float(-289.0);
    u_xlat5.y = (u_xlatb4.w) ? float(289.0) : float(-289.0);
    u_xlat5.z = (u_xlatb4.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat5.w = (u_xlatb4.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.x = (u_xlatb4.x) ? float(289.0) : float(-289.0);
    u_xlat4.y = (u_xlatb4.y) ? float(289.0) : float(-289.0);
    u_xlat4.z = (u_xlatb4.x) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.w = (u_xlatb4.y) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat20.xy = u_xlat3.zw * u_xlat5.zw;
    u_xlat3.xy = u_xlat3.xy * u_xlat4.zw;
    u_xlat3.xy = fract(u_xlat3.xy);
    u_xlat3.xy = u_xlat3.xy * u_xlat4.xy;
    u_xlat20.xy = fract(u_xlat20.xy);
    u_xlat20.xy = u_xlat20.xy * u_xlat5.xy;
    u_xlat21.x = u_xlat20.x * 34.0 + 1.0;
    u_xlat20.x = u_xlat20.x * u_xlat21.x;
    u_xlat21.x = u_xlat20.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb21 = !!(u_xlat21.x>=(-u_xlat21.x));
#else
    u_xlatb21 = u_xlat21.x>=(-u_xlat21.x);
#endif
    u_xlat21.xy = (bool(u_xlatb21)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat20.x = u_xlat20.x * u_xlat21.y;
    u_xlat20.x = fract(u_xlat20.x);
    u_xlat20.x = u_xlat21.x * u_xlat20.x + u_xlat20.y;
    u_xlat29 = u_xlat20.x * 34.0 + 1.0;
    u_xlat20.x = u_xlat20.x * u_xlat29;
    u_xlat29 = u_xlat20.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb29 = !!(u_xlat29>=(-u_xlat29));
#else
    u_xlatb29 = u_xlat29>=(-u_xlat29);
#endif
    u_xlat21.xy = (bool(u_xlatb29)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat20.x = u_xlat20.x * u_xlat21.y;
    u_xlat20.x = fract(u_xlat20.x);
    u_xlat20.x = u_xlat20.x * u_xlat21.x;
    u_xlat20.x = u_xlat20.x * 0.024390243;
    u_xlat20.x = fract(u_xlat20.x);
    u_xlat20.xy = u_xlat20.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat29 = floor(u_xlat20.y);
    u_xlat4.x = (-u_xlat29) + u_xlat20.x;
    u_xlat4.y = abs(u_xlat20.x) + -0.5;
    u_xlat20.x = dot(u_xlat4.xy, u_xlat4.xy);
    u_xlat20.x = inversesqrt(u_xlat20.x);
    u_xlat20.xy = u_xlat20.xx * u_xlat4.xy;
    u_xlat4 = u_xlat1.xyxy + vec4(-0.0, -1.0, -1.0, -0.0);
    u_xlat20.x = dot(u_xlat20.xy, u_xlat4.zw);
    u_xlat19.x = u_xlat19.x + (-u_xlat20.x);
    u_xlat21.xy = u_xlat1.xy * u_xlat1.xy;
    u_xlat21.xy = u_xlat1.xy * u_xlat21.xy;
    u_xlat22.xy = u_xlat1.xy * vec2(6.0, 6.0) + vec2(-15.0, -15.0);
    u_xlat22.xy = u_xlat1.xy * u_xlat22.xy + vec2(10.0, 10.0);
    u_xlat21.xy = u_xlat21.xy * u_xlat22.xy;
    u_xlat19.x = u_xlat21.y * u_xlat19.x + u_xlat20.x;
    u_xlat5 = u_xlat2.xyxy * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb5 = greaterThanEqual(u_xlat5, (-u_xlat5.zwzw));
    u_xlat5.x = (u_xlatb5.x) ? float(289.0) : float(-289.0);
    u_xlat5.y = (u_xlatb5.y) ? float(289.0) : float(-289.0);
    u_xlat5.z = (u_xlatb5.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat5.w = (u_xlatb5.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat2.xy = u_xlat2.xy * u_xlat5.zw;
    u_xlat2.xy = fract(u_xlat2.xy);
    u_xlat2.xy = u_xlat2.xy * u_xlat5.xy;
    u_xlat20.x = u_xlat2.x * 34.0 + 1.0;
    u_xlat2.x = u_xlat2.x * u_xlat20.x;
    u_xlat20.x = u_xlat2.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat20.x>=(-u_xlat20.x));
#else
    u_xlatb20 = u_xlat20.x>=(-u_xlat20.x);
#endif
    u_xlat20.xy = (bool(u_xlatb20)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat2.x = u_xlat20.y * u_xlat2.x;
    u_xlat2.x = fract(u_xlat2.x);
    u_xlat2.x = u_xlat20.x * u_xlat2.x + u_xlat2.y;
    u_xlat11.x = u_xlat2.x * 34.0 + 1.0;
    u_xlat2.x = u_xlat2.x * u_xlat11.x;
    u_xlat11.x = u_xlat2.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(u_xlat11.x>=(-u_xlat11.x));
#else
    u_xlatb11 = u_xlat11.x>=(-u_xlat11.x);
#endif
    u_xlat11.xy = (bool(u_xlatb11)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat2.x = u_xlat11.y * u_xlat2.x;
    u_xlat2.x = fract(u_xlat2.x);
    u_xlat2.x = u_xlat2.x * u_xlat11.x;
    u_xlat2.x = u_xlat2.x * 0.024390243;
    u_xlat2.x = fract(u_xlat2.x);
    u_xlat2.xy = u_xlat2.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat11.x = floor(u_xlat2.y);
    u_xlat5.x = (-u_xlat11.x) + u_xlat2.x;
    u_xlat5.y = abs(u_xlat2.x) + -0.5;
    u_xlat2.x = dot(u_xlat5.xy, u_xlat5.xy);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat2.xy = u_xlat2.xx * u_xlat5.xy;
    u_xlat1.x = dot(u_xlat2.xy, u_xlat1.xy);
    u_xlat10.x = u_xlat3.x * 34.0 + 1.0;
    u_xlat10.x = u_xlat3.x * u_xlat10.x;
    u_xlat2.x = u_xlat10.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2.x = !!(u_xlat2.x>=(-u_xlat2.x));
#else
    u_xlatb2.x = u_xlat2.x>=(-u_xlat2.x);
#endif
    u_xlat2.xy = (u_xlatb2.x) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat10.x = u_xlat10.x * u_xlat2.y;
    u_xlat10.x = fract(u_xlat10.x);
    u_xlat10.x = u_xlat2.x * u_xlat10.x + u_xlat3.y;
    u_xlat2.x = u_xlat10.x * 34.0 + 1.0;
    u_xlat10.x = u_xlat10.x * u_xlat2.x;
    u_xlat2.x = u_xlat10.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2.x = !!(u_xlat2.x>=(-u_xlat2.x));
#else
    u_xlatb2.x = u_xlat2.x>=(-u_xlat2.x);
#endif
    u_xlat2.xy = (u_xlatb2.x) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat10.x = u_xlat10.x * u_xlat2.y;
    u_xlat10.x = fract(u_xlat10.x);
    u_xlat10.x = u_xlat10.x * u_xlat2.x;
    u_xlat10.x = u_xlat10.x * 0.024390243;
    u_xlat10.x = fract(u_xlat10.x);
    u_xlat2.xy = u_xlat10.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat10.x = floor(u_xlat2.y);
    u_xlat3.x = (-u_xlat10.x) + u_xlat2.x;
    u_xlat3.y = abs(u_xlat2.x) + -0.5;
    u_xlat10.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat10.x = inversesqrt(u_xlat10.x);
    u_xlat2.xy = u_xlat10.xx * u_xlat3.xy;
    u_xlat10.x = dot(u_xlat2.xy, u_xlat4.xy);
    u_xlat10.x = (-u_xlat1.x) + u_xlat10.x;
    u_xlat1.x = u_xlat21.y * u_xlat10.x + u_xlat1.x;
    u_xlat10.x = (-u_xlat1.x) + u_xlat19.x;
    u_xlat1.x = u_xlat21.x * u_xlat10.x + u_xlat1.x;
    u_xlat1.x = u_xlat1.x + 0.497054994;
    u_xlat1.x = u_xlat1.x * 1.328125;
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    u_xlat2.xyz = u_xlat1.xxx * vec3(-0.446460903, -0.250646055, -0.96843648) + vec3(0.713278413, 0.285311401, 1.35522902);
    u_xlat2.w = 1.0;
    u_xlat1 = u_xlat16_1.wwww * u_xlat2;
    u_xlat1 = min(u_xlat0, u_xlat1);
    u_xlat1 = (-u_xlat0) + u_xlat1;
    u_xlat2.x = _TimeParameters.y * 0.5 + 1.0;
    u_xlat2.x = u_xlat2.x * 0.375 + 0.25;
    u_xlat0 = u_xlat2.xxxx * u_xlat1 + u_xlat0;
    u_xlat0 = u_xlat0 * vs_TEXCOORD1;
    u_xlat1.x = dot(_ShapeLightMaskFilter3, _ShapeLightMaskFilter3);
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(u_xlat1.x!=0.0);
#else
    u_xlatb1 = u_xlat1.x!=0.0;
#endif
    u_xlat2 = (-_ShapeLightInvertedFilter3) + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat10.x = dot(u_xlat2, _ShapeLightMaskFilter3);
    u_xlat19.xy = vs_TEXCOORD2.xy / vs_TEXCOORD2.ww;
    u_xlat2.xyz = texture(_ShapeLightTexture3, u_xlat19.xy).xyz;
    u_xlat10.xyz = u_xlat10.xxx * u_xlat2.xyz;
    u_xlat16_8.xyz = (bool(u_xlatb1)) ? u_xlat10.xyz : u_xlat2.xyz;
    u_xlat1.xyz = u_xlat16_8.xyz * _ShapeLightBlendFactors3.xxx;
    u_xlat2.xyz = u_xlat16_8.xyz * _ShapeLightBlendFactors3.yyy;
    u_xlat16_8.xyz = u_xlat0.xyz * u_xlat1.xyz + u_xlat2.xyz;
    u_xlat16_8.xyz = u_xlat16_8.xyz * vec3(_HDREmulationScale);
    u_xlat16_1.xyz = u_xlat16_8.xyz * vec3(vec3(_UseSceneLighting, _UseSceneLighting, _UseSceneLighting));
    u_xlat16_1.w = u_xlat0.w * _UseSceneLighting;
    u_xlat16_8.x = (-_UseSceneLighting) + 1.0;
    SV_TARGET0 = u_xlat16_8.xxxx * u_xlat0 + u_xlat16_1;
    return;
}

#endif
                              $GlobalsH         _TimeParameters                          _ShapeLightBlendFactors3                        _ShapeLightMaskFilter3                           _ShapeLightInvertedFilter3                    0      _HDREmulationScale                    @      _UseSceneLighting                     D          UnityPerMaterial         _DissolveAmount                              $GlobalsP         _ProjectionParams                            unity_MatrixVP                             UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @             _MainTex                  _ShapeLightTexture3                 UnityPerMaterial              UnityPerDraw          