<Q                         BLENDMODES_MODE_LIGHTERCOLOR   ETC1_EXTERNAL_ALPHA    PIXELSNAP_ON      #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _RendererColor;
uniform 	mediump vec2 _Flip;
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
vec2 u_xlat5;
void main()
{
    u_xlat0.xy = in_POSITION0.xy * _Flip.xy;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat1.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat5.xy = _ScreenParams.xy * vec2(0.5, 0.5);
    u_xlat1.xy = u_xlat5.xy * u_xlat1.xy;
    u_xlat1.xy = roundEven(u_xlat1.xy);
    u_xlat1.xy = u_xlat1.xy / u_xlat5.xy;
    gl_Position.xy = u_xlat0.ww * u_xlat1.xy;
    u_xlat1.xyz = u_xlat0.xyw * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD1.xy = u_xlat1.zz + u_xlat1.xy;
    gl_Position.zw = u_xlat0.zw;
    vs_TEXCOORD1.zw = u_xlat0.zw;
    u_xlat0 = in_COLOR0 * _Color;
    u_xlat0 = u_xlat0 * _RendererColor;
    vs_COLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	float _EnableExternalAlpha;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _AlphaTex;
UNITY_LOCATION(2) uniform mediump sampler2D _GrabTexture;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec3 u_xlat16_1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
mediump float u_xlat16_4;
float u_xlat9;
mediump float u_xlat16_9;
bool u_xlatb9;
void main()
{
    u_xlat0.xy = vs_TEXCOORD1.xy / vs_TEXCOORD1.ww;
    u_xlat0.xyz = texture(_GrabTexture, u_xlat0.xy).xyz;
    u_xlat16_1.x = dot(u_xlat0.xyz, vec3(0.298999995, 0.587000012, 0.114));
    u_xlat16_9 = texture(_AlphaTex, vs_TEXCOORD0.xy).x;
    u_xlat2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat9 = u_xlat16_9 + (-u_xlat2.w);
    u_xlat2.w = _EnableExternalAlpha * u_xlat9 + u_xlat2.w;
    u_xlat16_2 = u_xlat2 * vs_COLOR0;
    u_xlat16_4 = dot(u_xlat16_2.xyz, vec3(0.298999995, 0.587000012, 0.114));
#ifdef UNITY_ADRENO_ES3
    u_xlatb9 = !!(u_xlat16_4<u_xlat16_1.x);
#else
    u_xlatb9 = u_xlat16_4<u_xlat16_1.x;
#endif
    u_xlat16_1.xyz = (bool(u_xlatb9)) ? u_xlat0.xyz : u_xlat16_2.xyz;
    SV_Target0.xyz = u_xlat16_2.www * u_xlat16_1.xyz;
    SV_Target0.w = u_xlat16_2.w;
    return;
}

#endif
                               $Globals         _EnableExternalAlpha                             $Globals�         _ScreenParams                            _RendererColor                    �      _Flip                     �      _Color                    �      unity_ObjectToWorld                        unity_MatrixVP                   P             _MainTex               	   _AlphaTex                   _GrabTexture             